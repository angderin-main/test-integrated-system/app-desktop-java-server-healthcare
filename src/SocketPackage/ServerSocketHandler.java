package SocketPackage;

import helper.DatabaseConnect;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by Derin Anggara on 11/9/2017.
 */
public class ServerSocketHandler {
    private int portnumber;
    private ServerSocket serverSocket = null;
    private Socket clientSocket = null;
    private PrintWriter printStream = null;
    private BufferedReader dataInputStream = null;
    private String line = "";

    public ServerSocketHandler(int portnumber) {
        this.portnumber = portnumber;
    }

    public void OpenSocketClient() {
        System.out.println("Open Server Socket");
        try {
            serverSocket = new ServerSocket(portnumber);
            System.out.println("Server Socket Openned");
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    public void OpenService() {
        System.out.println("Listening To: " + portnumber);
        try
        {
            while (true){
                System.out.println("Ready!");
                clientSocket = serverSocket.accept();
                new Server(clientSocket).start();
            }
//            while (true){
//                System.out.println("Still Listening To: " + portnumber);
//                line = dataInputStream.readLine();
//                printStream.print("Ok" + "\r\n");
//            }
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    private class Server extends Thread {
        Socket socket;

        Server(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            super.run();
            PrintWriter printStreamThread = null;
            BufferedReader dataInputStreamThread = null;
            String type = null;
            String[] content = null;
            try {
                dataInputStreamThread = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                printStreamThread = new PrintWriter(socket.getOutputStream(), true);
                System.out.println("Accepting: " + portnumber);
                String textFromClient = dataInputStreamThread.readLine();
                System.out.println(textFromClient);

                String[] temp = textFromClient.split("#");
                String[] temp2 = temp[1].split(";");
                System.out.println("Type : "+temp[0]);
                for(String cnt: temp2){
                    System.out.println("Content : "+cnt);
                }
                type = temp[0];
                content = temp2;

                /*Database*/
                //login
                if(type.equals("login")){
                    DatabaseConnect db_conn = new DatabaseConnect();
                    db_conn.connect();
                    boolean val = db_conn.login(content[0],content[1]);
                    System.out.println(val);
                    db_conn.disconnect();
                    if (val){
                        printStreamThread.println("1");
                    }
                    else{
                        printStreamThread.println("0");
                    }
                }
                //login_admin
                else if(type.equals("login_admin")){
                    DatabaseConnect db_conn = new DatabaseConnect();
                    db_conn.connect();
                    boolean val = db_conn.login_admin(content[0],content[1]);
                    System.out.println(val);
                    db_conn.disconnect();
                    if (val){
                        printStreamThread.println("1");
                    }
                    else{
                        printStreamThread.println("0");
                    }
                }
                //signup_user
                else if(type.equals("signup_user")){
                    DatabaseConnect db_conn = new DatabaseConnect();
                    db_conn.connect();
                    boolean val = db_conn.signup_user(content[0],content[1],content[2],content[3],content[4]);
                    System.out.println(val);
                    db_conn.disconnect();
                    if (val){
                        printStreamThread.println("1");
                    }
                    else{
                        printStreamThread.println("0");
                    }
                }
                /*Database End*/
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
