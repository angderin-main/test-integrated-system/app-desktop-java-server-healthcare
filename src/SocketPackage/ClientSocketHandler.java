package SocketPackage;

import java.io.*;
import java.net.*;

/**
 * Created by Derin Anggara on 11/9/2017.
 */
public class ClientSocketHandler {

    private String host;
    private int portnumber;
    private Socket dataSocket = null;
    private PrintWriter dataOutputStream = null;
    private BufferedReader dataInputStream = null;
    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public ClientSocketHandler(String host, int portnumber) {
        this.portnumber = portnumber;
        this.host = host;
    }

    public void OpenSocketClient(){
        try {
            dataSocket = new Socket(host,portnumber);
            dataInputStream = new BufferedReader(new InputStreamReader(dataSocket.getInputStream()));
            dataOutputStream = new PrintWriter(dataSocket.getOutputStream(), true);
            System.out.println("Connection Open");
        } catch (UnknownHostException e) {
            System.err.println("Don't know about host: " + host);
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for the connection to: " + host);
        }
    }

    public String ConnectToServer(String data){
        if (dataSocket != null && dataInputStream != null && dataOutputStream != null){
            System.out.println("Connecting To Server");
            try {
                dataOutputStream.print(data + "\r\n");
                dataOutputStream.flush();
//                String responseLine;
//                System.out.println("Sending: " + data);
//                while ((responseLine = dataInputStream.readLine()) != null){
//                    System.out.println("Server: " + responseLine);
//                    if (responseLine.contains("Ok")){
//                        break;
//                    }
//                }

                String serverResponse = null;
                System.out.println("Waiting For Server");
                while ((serverResponse = dataInputStream.readLine()) != null){
                    System.out.println(serverResponse);
                }
                System.out.println(serverResponse);

                dataOutputStream.close();
                dataInputStream.close();
                dataSocket.close();

                return serverResponse;
            } catch (UnknownHostException e){
                System.err.println("Trying to connect to unknown host: " + e);
            } catch (IOException e) {
                System.err.println("IOException:  " + e);
            }
        }
        return null;
    }



}
