package helper;

import java.sql.*;

/**
 * Created by Derin Anggara on 11/10/2017.
 */
public class DatabaseConnect {
    // JDBC driver name and database URL
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://192.168.0.119/";

    //  Database credentials
    static final String USER = "root";
    static final String PASS = "";
    Connection conn = null;
    Statement stmt = null;

    public boolean login(String username, String password){
        try{
            this.use_database();
            System.out.println("login...");
            stmt = conn.createStatement();
            String sql = "SELECT * FROM login WHERE username='"+username+"' AND password='"+password+"'";
            System.out.println(sql);
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()){
                return true;
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return false;
    }

    public boolean login_admin(String username, String password){
        try{
            this.use_database();
            System.out.println("login user...");
            stmt = conn.createStatement();
            String sql = "SELECT * FROM login_admin WHERE username='"+username+"' AND password='"+password+"'";
            System.out.println(sql);
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()){
                return true;
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return false;
    }

    public boolean signup_user(String username, String password, String nomor_ktp, String fullname, String email){
        try{
            this.use_database();
            System.out.println("signup user...");
            stmt = conn.createStatement();
            String sql = "INSERT INTO profile (no_ktp,fullname,email) VALUES ('"+nomor_ktp+"','"+fullname+"','"+email+"')";
            System.out.println(sql);
            stmt.execute(sql);

            this.use_database();
            System.out.println("signup user 2...");
            stmt = conn.createStatement();
            String sql2 = "INSERT INTO login (username, password, email) VALUES ('"+username+"','"+password+"','"+email+"')";
            System.out.println(sql2);
            stmt.execute(sql2);

            return true;

        }
        catch(Exception e){
            e.printStackTrace();
        }
        return false;
    }

    public String list_alergi(String username){
        try{
            this.use_database();
            System.out.println("list alergi...");
            stmt = conn.createStatement();
            String sql = "SELECT allergy.allergy_name FROM allergy INNER JOIN profile ON allergy.id_profile = "+username+"";
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return "";
    }

    public void use_database(){
        try{
            System.out.println("using database...");
            stmt = conn.createStatement();
            String sql = "USE healthmation";
            System.out.println(sql);
            stmt.executeQuery(sql);
        }
        catch(Exception e){

        }
    }

    public void connect(){
        try{
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
        }
        catch(Exception se){
        }
    }

    public void disconnect(){
        try{
            if(stmt!=null)
                stmt.close();
        }
        catch(SQLException se2){
        }
        try{
            if(conn!=null)
                conn.close();
        }
        catch(SQLException se){
            se.printStackTrace();
        }
        System.out.println("database disconnected");
    }
}
