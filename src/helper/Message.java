package helper;

/**
 * Created by Derin Anggara on 11/9/2017.
 */
public class Message {
    private String type;
    private String[] value;
    private String temp;

    public Message(String type, String[] value){
        this.type = type;
        this.value = value;
    }

    @Override
    public String toString(){
        this.temp = "";
        for (String y: value ) {
            this.temp += y;
            this.temp += ";";
        }
        this.temp = this.type+"#"+this.temp;
        int count = this.temp.length();
        this.temp = this.temp.substring(0,count-1);
        return this.temp;
    }
}
