package sample;

import helper.DatabaseConnect;
import helper.Message;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import SocketPackage.ServerSocketHandler;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Thread serverThread = new Thread(new Server());
        serverThread.start();
    }

    public static class Server implements Runnable{

        @Override
        public void run() {
            int portnumber = 9999;
            ServerSocketHandler serverSocketHandler = new ServerSocketHandler(portnumber);
            serverSocketHandler.OpenSocketClient();
            serverSocketHandler.OpenService();
        }
    }


    public static void main(String[] args) {
        launch(args);
    }
}
